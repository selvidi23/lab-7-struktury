#include <stdio.h>
#include <math.h>
// Proszę napisać program do operowania na punktach 2D z następującymi możliwościami:
// pamiętanie dla każdego punktu nazwy (litery od A do Z) i współrzędnych x,y (wszystko podaje użytkownik)
// dopisywanie kolejnych punktów do bazy (maks. 100 punktów), można to robić "na raty"
// wyświetlanie wszystkich wprowadzonych punktów (bez "pustych miejsc")
// modyfikacja wybranego punktu (punkt jest wybierany indeksem)
// modyfikacja wybranego punktu (punkt jest wybierany nazwą)
// usuwanie wybranego punktu (punkt jest wybierany nazwą)
// liczenie odległości między dwoma punktami (punkt jest wybierany nazwą)
// liczenie długości linii łamanej o podanych punktach (punkt jest wybierany nazwą)
// kontrola unikalności nazw (niedopuszczenie do powtarzania nazw punktów, np. dwa punkty o nazwie A)

struct Point
{
    char id;
    int x,y;
}tab[100];

int counter = 0;


int search_Id(char id){
    
    for(int i =0;i<counter;i++){
        if(tab[i].id==id){
            return i;
        }
    }
    return 404;
}


void add_Point(){
    int a;
    int tmp = counter;
    printf("Podaj ile punktow chcesz dodac \n");
    scanf("%d",&a);
    
    for(int i=counter;i<tmp+a;i++){
        printf("Podaj id (znaki) \n");
        do{
            scanf(" %c",&tab[i].id);
            if(search_Id(tab[i].id)!=404){
                printf("Id nie może sie powtarzać \n");
            }
        }while(search_Id(tab[i].id)!=404);
        
        printf("Podaj x \n");
        scanf("%d",&tab[i].x);
        printf("Podaj y\n");
        scanf("%d",&tab[i].y);
        counter++;
    }

}

void print_Points(){
    for(int i = 0;i<counter;i++){
        printf("Punkt %c ma wspolrzedne X = %d Y = %d\n",tab[i].id,tab[i].x,tab[i].y);
    }
}

void modify_Point_By_Index(int index, char id, int x,int y){
    tab[index].id = id;
    tab[index].x = x;
    tab[index].y = y;
}

void modify_Point_By_Id(char id,int x, int y){
    int tmp;

    if(search_Id(id)!=404){
        tab[search_Id(id)].x = x;
        tab[search_Id(id)].y = y;
    }
}

void delete_Point_By_Id(char id){
    if(search_Id(id)!=404){
        for(int i= search_Id(id);i<counter-1;i++){
            tab[i].id = tab[i+1].id;
            tab[i].x = tab[i+1].x;
            tab[i].y = tab[i+1].y;
        }
        counter--;
    }
}

// d=sqrt(pow(x2 -x1)+ pow(y2-y1))

float calc_Distance_For_Points(char a,char b){
    if(search_Id(a)!=404 && search_Id(b)!=404){
        int p1=search_Id(a),p2=search_Id(b);
        return sqrt(pow((float)tab[p2].x - (float)tab[p1].x , 2) + pow((float)tab[p2].y - (float)tab[p1].y, 2));
    }
}

float calc_Distance_For_Next_Points(int n){
    char tmp[n];
    float result=0;
    printf("Podaj Znaki\n");
    for(int i=0;i<n;i++){
        scanf(" %c",&tmp[i]);
    }
    for(int i=0;i<n;i++){
        if(i+1<n){
            result=result+calc_Distance_For_Points(tmp[i],tmp[i+1]);
        }
            
    }
    return result;
}

int menu(){
  
    int liczba;
    printf("\n");
    printf("Wybierz opcje \n");
    printf(" \n");

    printf("1. Dodaj Punkt \n");
    printf("2. Wyświetl Punkty  \n");
    printf("3. Modyfikacja Punktu przez indeks \n");
    printf("4. Modyfikacja Punktu przez Nazwe \n");
    printf("5. Usuwanie Punktu przez Nazwe  \n");
    printf("6. Obliczanie odleglosci dla 2 punktow \n");
    printf("7. Oblicznie odlegosci dla lamanej lini  \n");
    printf("0. Koniec Programu \n");
    printf(" \n");

    scanf("%d",&liczba);

    return liczba;
}

int main(){

    int a,b,c,select;
    char letter,letter2;

    do{
        select = menu();
        switch (select)
        {
        case 1:
            add_Point();
            break;
        case 2:
            print_Points();
            break;
        case 3:
            printf("Podaj Indeks \n");
            scanf("%d",&c);
            printf("Podaj Id (Litera) \n");
            scanf(" %c",&letter);
            printf("Podaj x\n");
            scanf("%d",&a);
            printf("Podaj y\n");
            scanf("%d",&b);
            modify_Point_By_Index(c,letter,a,b);
            break;
        case 4:
            printf("Podaj id(Litera)\n");
            scanf(" %c",&letter);
            printf("Podaj x\n");
            scanf("%d",&a);
            printf("Podaj y\n");
            scanf("%d",&b);
            modify_Point_By_Id(letter,a,b);
            break;
        case 5:
            printf("Podaj id(Litera)\n");
            scanf(" %c",&letter);
            delete_Point_By_Id(letter);
            break;
        case 6:
            printf("Podaj id(Litera)\n");
            scanf(" %c",&letter);
            printf("Podaj id(Litera)\n");
            scanf(" %c",&letter2);
            printf("Odlegosc to = %f\n", calc_Distance_For_Points(letter,letter2));

            break;
        case 7:
            printf("Podaj ile punktow\n");
            scanf("%d",&a);
            printf("Odległosc wynosi = %f\n",calc_Distance_For_Next_Points(a));
            break;
        }
    }while(select!=0); 

    return 0;


}